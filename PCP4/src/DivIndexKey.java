// Imports
import edu.rit.io.InStream;
import edu.rit.io.OutStream;
import edu.rit.io.Streamable;
import java.io.IOException;

/**
 * This class stores the key used in DivIndex to differentiate entries
 * @author Aziel Shaw
 * @version 11/30/1018
 */
public class DivIndexKey implements Streamable { 
    
    // Instance variables
    private String state;
    private String county;
    
    public DivIndexKey() {
        state = "";
        county = "";
    }
    
    /**
     * Constructs a DivIndexKey object with the State/County pair
     * @param s The state
     * @param c The county
     */
    public DivIndexKey(String s, String c) {
        this.state = s;
        this.county = c;
    }
    
    /**
     * Checks if another key object is equivalent to this key.
     * @param obj The other object
     * @return If the objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        return state.equals(((DivIndexKey) obj).getState()) && 
                county.equals(((DivIndexKey) obj).getCounty());
    }

    /**
     * Returns the county contained in this key
     * @return The county contained in this key
     */
    public String getCounty() {
        return county;
    }

    /**
     * Returns the state contained in this key
     * @return The state contained in this key
     */
    public String getState() {
        return state;
    }
    
    /**
     * Returns the pseudo-unique hashcode value of this object.
     * @return The hashcode for this key.
     */
    @Override
    public int hashCode() {
        String toHash = getState() + getCounty();
        char[] array = toHash.toCharArray();
        int result = 0;
        for(int i = 0; i < array.length; i++)
            result += (i + 1) * array[i];
        return result;
    }

    @Override
    public void writeOut(OutStream out) throws IOException {
        out.writeString(state);
        out.writeString(county);
    }

    @Override
    public void readIn(InStream in) throws IOException {
        this.state  = in.readString();
        this.county = in.readString();
    }
}
