// Imports
import edu.rit.io.InStream;
import edu.rit.io.OutStream;
import edu.rit.pj2.Tuple;
import edu.rit.pj2.Vbl;
import java.io.IOException;

/**
 * Reduction Variable class for reducing Diversity Indexes
 * @author Aziel Shaw
 * @version 11/30/1018
 */
public class DivIndexVbl extends Tuple implements Vbl {

    // Kludge to avert false sharing in multithreaded programs.
    // Padding fields.
    volatile long p0 = 1000L;
    volatile long p1 = 1001L;
    volatile long p2 = 1002L;
    volatile long p3 = 1003L;
    volatile long p4 = 1004L;
    volatile long p5 = 1005L;
    volatile long p6 = 1006L;
    volatile long p7 = 1007L;
    volatile long p8 = 1008L;
    volatile long p9 = 1009L;
    volatile long pa = 1010L;
    volatile long pb = 1011L;
    volatile long pc = 1012L;
    volatile long pd = 1013L;
    volatile long pe = 1014L;
    volatile long pf = 1015L;

    // Method to prevent the JDK from optimizing away the padding fields.
    long preventOptimization() {
        return p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7
                + p8 + p9 + pa + pb + pc + pd + pe + pf;
    }
    
    // Instance variables
    protected int[] values; 
    protected String state;
    protected String county;
    
    /**
     * Empty constructor
     */
    public DivIndexVbl() {
        values = new int[7];
        state = "";
        county = "";
    }
    
    /**
     * Constructor 
     * @param v Values of different diversity indexes
     * @param s State this object owns
     * @param c County this object owns
     */
    public DivIndexVbl(int[] v, String s, String c) {
        this.values = v;
        this.state = s;
        this.county = c;
    }
    
    /**
     * Returns the diversity indexes
     * @return The diversity indexes
     */
    public int[] getValues() {
        return this.values;
    }
    
    /**
     * Returns the State value
     * @return The State value
     */
    public String getState() {
        return this.state;
    }
    
    /**
     * Returns the County value
     * @return The County value
     */
    public String getCounty() {
        return this.county;
    }
    
    /**
     * Sets the diversity index values
     * @param v New index values
     */
    private void setValues(int[] v) {
        this.values = v;
    }
    
    /**
     * Sets the State string
     * @param s The State string
     */
    private void setState(String s) {
        this.state = s;
    }
    
    /**
     * Sets the County string
     * @param c The county string
     */
    private void setCounty(String c) {
        this.county = c;
    }
    
    /**
     * Sets all the values of the object to what's passed in.
     * @param v New index values
     * @param s The State string
     * @param c The county string
     */
    public void setItem(int[] v, String s, String c) {
        this.setValues(v);
        this.setState(s);
        this.setCounty(c);
    }
    
    /**
     * Returns the diversity index of this State/StateCounty combo
     * @return The diversity index
     */
    public double getDiversityIndex() {
        double secondTerm = 0;
        for (int j = 0; j < 6; j++) {
            secondTerm += ((long) values[j] * (values[6] - values[j]));
        }
        return (1 / Math.pow(values[6], 2)) * secondTerm;
    }
    
    /**
     * Writes this object into the out stream
     * @param out The out stream
     * @throws IOException Throws IO Exception
     */
    @Override
    public void writeOut(OutStream out) throws IOException {
        out.writeIntArray(this.values); 
        out.writeString(this.state);
        out.writeString(this.county);
    }

    /**
     * Sets this objects values to the passed in objects values.
     * @param in The in stream
     * @throws IOException Throws IO Exception
     */
    @Override
    public void readIn(InStream in) throws IOException {
        this.setValues(in.readIntArray());
        this.setState(in.readString());
        this.setCounty(in.readString());
    }

    /**
     * Sets this vbl to the values of the passed in vbls values
     * @param vbl The vbl to make this vbls 
     */
    @Override
    public void set(Vbl vbl) {
        DivIndexVbl temp = (DivIndexVbl) vbl;
        this.setItem(temp.getValues(), temp.getState(), temp.getCounty());
    }

    /**
     * Performs a summation reduction to this vbl
     * @param vbl The vbl to reduce
     */
    @Override
    public void reduce(Vbl vbl) {
        // Cast the Vbl to DivIndexVbl
        DivIndexVbl temp = (DivIndexVbl) vbl;
        // Get the vbls values
        int[] itr = temp.getValues();
        // Iterate over values and add to 
        for (int i = 0; i < itr.length; i++) {
            this.values[i] += itr[i];
        }
        // Sets this Vbls String values to the passed in Vbls
        this.state = temp.getState();
        if(this.county.equals("STATE"))
            this.county = "STATE";
        else
            this.county = temp.getCounty();
    }
}
