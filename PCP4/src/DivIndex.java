// Imports
import edu.rit.pjmr.Combiner;
import edu.rit.pjmr.Customizer;
import edu.rit.pjmr.Mapper;
import edu.rit.pjmr.PjmrJob;
import edu.rit.pjmr.Reducer;
import edu.rit.pjmr.TextFileSource;
import edu.rit.pjmr.TextId;
import java.io.IOException;
import java.util.HashSet;

/**
 * This class DivIndex calculates the Diversity Index of states that are passed
 * in as commandline arguments.
 * 
 * @author Aziel Shaw
 * @version 11/30/1018
 */
public class DivIndex extends PjmrJob<TextId, String, DivIndexKey, DivIndexVbl> {

    // Class instant variables
    private static String[] nodes;
    private static String fileName;
    private static Integer year;

    /**
     * Main method
     *
     * @param args commandline arguments
     * @throws Exception Throws exception
     */
    @Override
    public void main(String[] args) throws Exception {
        // Commandline Argumentts
        if (args.length < 3) {
            System.out.println("Must contain at least three arguments.");
            System.out.println("Required: <nodes> <file> <year> ");
            System.out.println("Optional: \"<state>\"");
            usage();
        }
        // Check arguments are viable
        try {
            nodes = args[0].split("\\,");
            fileName = args[1];
            year = Integer.parseInt(args[2]);
        } catch (NumberFormatException nfe) {
            System.out.println("<year> must be an integer");
            usage();
        } catch (NullPointerException npe) {
            System.out.println("<file> must point to file");
            usage();
        } catch (Exception e) {
            usage();
        }
        // Check file is ok
        TextFileSource tfs = null;
        try {
            tfs = new TextFileSource(fileName);
            tfs.open();
            tfs.close();
        } catch(IOException e) {
            System.out.println("<file> must point to file");
            usage();
        } 
        // Check year is ok
        if (year < 1 || year > 10) {
            System.out.println("Year must be between 1 and 10 (inclusive)");
            usage();
        }
        // Check states are viable
        HashSet<String> set = new HashSet<>();
        HashSet<String> allStates = getAllStates();
        for(int i = 3; i < args.length; i++) {
            if(!set.add(args[i])) {
                System.out.println("Cannot pass in states multiple times!");
                usage();
            }
        }
        // get Number of Threads
        int NT = Math.max(threads(), 1);
        // Set up Mapper Task
        for (String node : nodes) {
            mapperTask(node)
                    .source(new TextFileSource(fileName))
                    .mapper(NT, DivIndexMapper.class, args);
        }
        // Set up Reducer Task
        reducerTask()
                .customizer(DivIndexCustomizer.class)
                .reducer(DivIndexReducer.class);
        // Start Job
        startJob();
    }

    /**
     * Print usage text and exit
     */
    private static void usage() {
        System.out.println("java pj2 jar=<jar> threads=<NT> DivIndex <nodes> "
                + "<file> <year> [ \"<state>\" ... ]");
        System.out.println("<jar> is the name of the JAR file containing all "
                + "of the program's Java class files.");
        System.out.println("<NT> is the number of mapper threads per mapper "
                + "task; if omitted, the default is 1.");
        System.out.println("<nodes> is a comma-separated list of cluster node "
                + "names on which to run the analysis. One or more node names "
                + "must be specified.");
        System.out.println("<file> is the name of the file on each node's "
                + "local hard disk containing the census data to be analyzed.");
        System.out.println("<year> is the year to be analyzed. It must be "
                + "an integer from 1 to 10.");
        System.out.println("<state> is a state name to be analyzed. There can "
                + "be zero or more state names. A particular state name must "
                + "not appear more than once.");
        terminate(1);
    }

    /**
     * This method returns a string array of all states
     * @return String array containing all states in alphabetical order
     */
    private static HashSet<String> getAllStates() {
        String[] arr =  new String[]{"Alabama", "Alaska", "Arizona", "Arkansas",
            "California", "Colorado", "Connecticut", "Delaware",
            "District Of Columbia", "Florida", "Georgia", "Hawaii", "Idaho",
            "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine",
            "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi",
            "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
            "New Jersey", "New Mexico", "New York", "North Carolina",
            "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania",
            "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
            "Utah", "Vermont", "Virginia", "Washington", "West Virginia",
            "Wisconsin", "Wyoming"};
        HashSet<String> set = new HashSet<>();
        for(String s : arr)
            set.add(s);
        return set;
    }

    /**
     * Mapper class.
     */
    private static class DivIndexMapper
            extends Mapper<TextId, String, DivIndexKey, DivIndexVbl> {

        // Mapper instance variables
        private HashSet<String> states;
        private static final Integer AGEGROUP = 0;
        private static Integer year;
        private static final int[] indexes = new int[]{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};

        /**
         * Setup initial values from the commandline
         * @param args commandline arguments
         * @param combiner combiner to add to map
         */
        public void start(String[] args,
                Combiner<DivIndexKey, DivIndexVbl> combiner) {
            year = Integer.parseInt(args[2]);

            // Get states if any.
            states = new HashSet<String>();
            for (int i = 3; i < args.length; i++) {
                states.add(args[i]);
            }
            if (states.isEmpty()) {
                states = getAllStates();
            }
        }

        /**
         * This method looks at data from the dataset, and then
         * @param id File entry
         * @param contents Current line from file.
         * @param combiner Combiner which maps DivIndexKeys to DivIndexVbls
         */
        public void map(TextId id, // File name and line number
                String contents, // Line from file
                Combiner<DivIndexKey, DivIndexVbl> combiner) {
            // Splitt contents into array
            String[] contentsArry = contents.split("\\,");
            String stateName = contentsArry[3]; // Get state name
            String countyName = contentsArry[4]; // Get county name
            DivIndexKey countyKey = new DivIndexKey(stateName, countyName); // Create countyKey
            DivIndexKey stateKey = new DivIndexKey(stateName, "STATE"); // Create stateKey
            Integer contentsYear = Integer.parseInt(contentsArry[5]); // Get year of current line
            // Create array of data from indexes
            int[] data = new int[indexes.length];
            for (int i = 0; i < data.length; i++) {
                data[i] = Integer.parseInt(contentsArry[indexes[i]]);
            }
            // Check that the current index in the file being checked exists
            //     within the passed in states
            if (states.contains(stateName)
                    && year.compareTo(contentsYear) == 0
                    && AGEGROUP.compareTo(Integer.parseInt(contentsArry[6])) == 0) {
                // Assign values 
                int[] values = new int[(indexes.length / 2) + 1];
                values[0] = data[0] + data[1];
                values[1] = data[2] + data[3];
                values[2] = data[4] + data[5];
                values[3] = data[6] + data[7];
                values[4] = data[8] + data[9];
                values[5] = data[10] + data[11];
                values[6] = values[0] + values[1] + values[2] + values[3] + values[4] + values[5];
                // Create Vbl object
                DivIndexVbl newVbl = new DivIndexVbl(values, stateName, countyName);
                // At county map to combiner
                combiner.add(countyKey, newVbl);
                // Add county values to state
                combiner.add(stateKey, newVbl);
            }
        }
    }

    /**
     * Reducer task customizer class.
     */
    private static class DivIndexCustomizer
            extends Customizer<DivIndexKey, DivIndexVbl> {

        /**
         * Checks if a value (<v1>) comes before another value (<v2>) and 
         * returns true.
         * @param k1 The key associated with value 1
         * @param v1 value 1
         * @param k2 The key associated with value 2
         * @param v2 value 2
         * @return If K1/V1 are "ordered" before K2/V2
         */
        @Override
        public boolean comesBefore(DivIndexKey k1, DivIndexVbl v1,
                DivIndexKey k2, DivIndexVbl v2) {
            
            // If the state is lexographically before the other state
            if(k1.getState().compareTo(k2.getState()) < 0)
                return true;
            // If the state is lexographically after the other state
            else if(k1.getState().compareTo(k2.getState()) > 0)
                return false;
            // If the states are equal
            else {
                if(k1.getCounty().equals("STATE")) {
                    return true;
                } else if(k2.getCounty().equals("STATE")) {
                    return false;
                }
                // If this objects Diversity index is greater than the others
                if(v1.getDiversityIndex() > v2.getDiversityIndex()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Reducer class.
     */
    private static class DivIndexReducer
            extends Reducer<DivIndexKey, DivIndexVbl> {

        /**
         * Prints results
         *
         * @param key Key of Result
         * @param value Value of result
         */
        public void reduce(DivIndexKey key, // State/County Name
                DivIndexVbl value) { // Diversity Index
            // If the county is they keyword for the state, print the state info
            if (key.getCounty().equals("STATE")) {
                System.out.printf("%s\t\t%.5g%n", key.getState(), value.getDiversityIndex());
            // Otherwise, print county info
            } else {
                System.out.printf("\t%s\t%.5g%n", key.getCounty(), value.getDiversityIndex());
            }
        }
    }
}
