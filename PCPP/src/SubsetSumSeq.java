import edu.rit.pj2.Task;

/**
 *
 * @author Aziel Shaw - Tushar Iyer
 * @version October 24th, 2018
 */
public class SubsetSumSeq extends Task {

    @Override
    public void main(String[] strings) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("Usage: java pj2 "
                + "jar=proj.jar SubsetSumSeq <filename>");
        terminate(1);
    }
    
    
    /**
     * Specifies that this task requires at least 1 core
     * @return Number of cores required
     */
    protected static int coresRequired() {
        return 1;
    }
    
}
