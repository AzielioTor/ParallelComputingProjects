/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1_practice;

import edu.rit.pj2.LongLoop;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;

/**
 *
 * @author azieliotor
 */
public class CollatzSmp extends Task {

   @Override
    public void main(String[] args) throws Exception {
        System.out.println("Starting Collatz Sequential...");
        if(args.length < 2) {
            usage();
        }
        String lbnd = args[0];
        String ubnd = args[1];
        System.out.println(lbnd + " | " + ubnd);
        long lb = Long.parseLong(lbnd);
        long ub = Long.parseLong(ubnd);
        if(lb > ub) {
            System.err.println("CollatzSeq: <ub> must be >= <lb>");
            usage();
        }
        if(lb < 0) {
            System.err.println("CollatzSeq: <lb> must be >= 1");
            usage();
        }
        long max_n = 0;
        long max_index = 0;
        long min_n = Long.MAX_VALUE;
        long min_index = Long.MAX_VALUE;
        System.out.println(lb + " | " + ub);
        
        // Parallel FOR
        parallelFor (lb, ub).exec(new LongLoop() {
            @Override
            public void run(long l) throws Exception {
                for(long i = lb; i <= ub; i++) {
                    long result = collatz(i);
                    if(result > max_n) {
                        max_n = result;
                        max_index = l;
                    }
                    if(result < min_n) {
                        min_n = result;
                        min_index = l;
                    }
                }
            }
            
        });
//        for(long i = lb; i <= ub; i++) {
//            long result = collatz(i);
//            if(result > max_n) {
//                max_n = result;
//                max_index = i;
//            }
//            if(result < min_n) {
//                min_n = result;
//                min_index = i;
//            }
//        }
        System.out.println("C(" + min_index + ") = " + min_n);
        System.out.println("C(" + max_index + ") = " + max_n);
    }
    
    private static long collatz(long num) {
        long counter = 1;
        while(num > 1) {
            if(num % 2 == 0) {
                num = (num / 2);
            } else {
                num = (3 * num) + 1;
            }
            counter++;
        }
        return counter; 
    }
    
    private static void usage() {
        System.err.println ("Usage: java -cp pj2.jar pj2 debug=makespan jar=Project1_Practice.jar Project1_Practice.CollatzSeq <lb> <ub>");
        terminate(1);
    }
    
}
