/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1_practice;

/**
 *
 * @author azieliotor
 */
public class CollatzInfoPair {
    private long number;
    private long index;
    
    public CollatzInfoPair(long n, long i) {
        this.number = n;
        this.index = i;
    }
    
    public long getNumber() {
        return this.number;
    }
    
    public long getIndex() {
        return this.index;
    }
}
