// Imports
import edu.rit.pj2.Task;
import static edu.rit.pj2.Task.terminate;

/**
 * Calculates the lemonine conjecture with the lowest p 
 * through a range of numbers sequentially. Returns the
 * n of all numbers with the highest p.
 * @author Aziel Shaw
 * @version 18-Sept-2018
 */
public class LemoineSeq extends Task {
    
    /**
     * Launches the Sequential Lemoine Conjecture Program
     * @param args The <lower> and <upper> bounds at which to iterate over
     * @throws Exception Throws an Exception if an error pops up
     */
    @Override
    public void main(String[] args) throws Exception {
        if(args.length < 2) {
            System.out.println("LemoineSeq: Must have 2 arguments passed in, <lb>, <ub>");
            usage();
        }
        // Cast commandline arguments to ints
        int lb = parseInput(args[0]);
        int ub = parseInput(args[1]);
        // Check commandline arguments worked correctly
        if(lb == -2 || ub == -2) {
            usage();
        }
        // Check that <ub> is larger than <lb>
        else if(lb > ub) {
            System.err.println("LemoineSeq: <ub> must be >= <lb>");
            usage();
        }
        // Check that <lb> is greater than or equal to 1
        else if(lb <= 5) {
            System.err.println("LemoineSeq: <lb> must be > 5");
            usage();
        }
        // Check that <lb> is an odd number
        else if(lb % 2 == 0) {
            System.err.println("LemoineSeq: <lb> must be odd");
            usage();
        }
        // Check that <ub> is an odd number
        else if(ub % 2 == 0) {
            System.err.println("LemoineSeq: <ub> must be odd");
            usage();
        }
        // Create ArrayList to store results from method <I>lemoine</I>
        LemoineValuePairVbl.Min lvp = new LemoineValuePairVbl.Min(null);
        // Iterate through bounds
        for(int i = lb; i <= ub; i=i+2) {
            if(i % 2 == 1) {
                // If the object isn't null, perform computation
                if(lvp != null) {
                    lvp.reduce(compareLemoine(lvp.getItem(), lemoine(i)));
                } else {
                    lvp.reduce(new LemoineValuePair(-1, -1, -1));
                }
            }
        }
        if(lvp == null) {
            lvp.reduce(new LemoineValuePair(-1, -1, -1));
        }
        // Sort array to find largest <I>p</I> value
        int n = lvp.getItem().getN();
        int p = lvp.getItem().getP();
        int q = lvp.getItem().getQ();
        // Print result
        System.out.println(n + " = " + p + " + 2*" + q);
    }
    
    /**
     * Takes input and returns the longs if proper.
     * If not proper it catches the thrown exceptions and exits.
     * @param arg The string argument to convert into an int
     * @return The parsed integer
     */
    private static int parseInput(String arg) {
        int result = -2;
        try {
            result = Integer.parseInt(arg);
        } catch(NumberFormatException nfe) {
            System.err.println("Arguments passed in must be integers");
        } catch(Exception e) {
            System.err.println("Arguments passed in must be integers");
        } finally {
            return result;
        }
    }
    
    /**
     * Perform the Lemoine Conjecture on a number
     * @param num Number to check for p + 2*q
     * @return n, p, and q that satisfies n = p + 2*q. 
     *         Where p is the smallest value to satisfy.
     */
    private static LemoineValuePair lemoine(int num) {
        Prime.Iterator primeIterator = new Prime.Iterator();
        int p, q;
        if(num > 5) {
            while(true) {
                // Iterate until a value is found to satisfy Lemoines
                p = primeIterator.next();
                q = (num - p) / 2;
                // If both numbers are prime, return
                if(Prime.isPrime(q) && Prime.isPrime(p)) {
                    return new LemoineValuePair(num, p, q);
                }
            }
        }
        return  null;
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("Usage: java pj2 "
                + "jar=PCP1.jar lemoine.LemoineSeq <lb> <ub>");
        terminate(1);
    }
    
    /**
     * Compares two Lemoine objects
     * @param p1 Object 1 to compare to Object 2
     * @param p2 The object to be compared to Object 1
     * @return The object with the lower p value
     */
    private static LemoineValuePair compareLemoine(LemoineValuePair p1, LemoineValuePair p2) {
        // Check for null objects
        if(p1 == null) {
            if(p2 == null) {
                return null;
            }
            return p2;
        } else if(p2 == null) {
            return p1;
        }
        // Start comparision
        if(p1.compareTo(p2) == 1) {
            return p1;
        } else if(p1.compareTo(p2) == -1) {
            return p2;
        } else {
            return p1;
        }
    }
    
    /**
     * Specifies that this task requires at least 1 core
     * @return Number of cores required
     */
    protected static int coresRequired() {
        return 1;
    }
}