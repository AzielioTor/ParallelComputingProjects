// Imports
import edu.rit.io.InStream;
import edu.rit.io.OutStream;
import edu.rit.pj2.Tuple;
import edu.rit.pj2.Vbl;
import java.io.IOException;

/**
 * Provides a global reduction variable for a group of integers stored
 * for the purpose of helping solve the Lemoine Conjecture
 * @author Aziel Shaw
 * @version 18-Sept-2018
 */
public class LemoineValuePairVbl extends Tuple implements Vbl {

// Kludge to avert false sharing in multithreaded programs.
    // Padding fields.
    volatile long p0 = 1000L;
    volatile long p1 = 1001L;
    volatile long p2 = 1002L;
    volatile long p3 = 1003L;
    volatile long p4 = 1004L;
    volatile long p5 = 1005L;
    volatile long p6 = 1006L;
    volatile long p7 = 1007L;
    volatile long p8 = 1008L;
    volatile long p9 = 1009L;
    volatile long pa = 1010L;
    volatile long pb = 1011L;
    volatile long pc = 1012L;
    volatile long pd = 1013L;
    volatile long pe = 1014L;
    volatile long pf = 1015L;

    // Method to prevent the JDK from optimizing away the padding fields.
    long preventOptimization() {
        return p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7
                + p8 + p9 + pa + pb + pc + pd + pe + pf;
    }

    /**
     * The shared LemoineValuePair item.
     */
    protected LemoineValuePair lvp;

    /**
     * Null constructor for class LemoineValuePairVbl
     */
    public LemoineValuePairVbl() {
        this(null);
    }

    /**
     * Constructor for LemoineValuePairVbl
     * @param lvp Initial lvp item for object
     */
    public LemoineValuePairVbl(LemoineValuePair lvp) {
        this.lvp = lvp;
    }

    /**
     * Returns the local LemoineValuePair Object
     * @return The local LemoineValuePair Object
     */
    public LemoineValuePair getItem() {
        return this.lvp;
    }
    
    /**
     * Sets the local LemoineValuePair Object
     * @return The new LemoineValuePair Object to set
     */
    public void setItem(LemoineValuePair lvp) {
        this.lvp = lvp;
    }

    /**
     * Write this LemoineValuePair reduction variable to the given out stream.
     * @param out Out Stream
     * @throws IOException Thrown if an I/O error occurred.
     */
    @Override
    public void writeOut(OutStream out) throws IOException {
        out.writeObject(this.getItem());
    }

    /**
     * Read this LemoineValuePair reduction variable from the given in stream.
     * @param in In stream.
     * @throws IOException Thrown if an I/O error occurred.
     */
    @Override
    public void readIn(InStream in) throws IOException {
        this.setItem((LemoineValuePair) in.readObject());
    }

    /**
     * Set this shared variable to the given shared variable.
     * @param vbl Shared variable.
     */
    @Override
    public void set(Vbl vbl) {
        this.setItem(((LemoineValuePairVbl) vbl).getItem());
    }

    /**
     * Reduce the given LemoineValuePair into this shared variable. 
     * This shared variable's LemoineValuePair field and the 
     * <TT>lvp</TT> argument are combined together using this 
     * shared variable's reduction operation, and
     * the result is stored in the local lvp field.
     * <P>
     * The base class <TT>reduce()</TT> method throws an exception. The
     * reduction operation must be defined in a subclass's <TT>reduce()</TT>
     * method.
     *
     * @param  lvp  LemoineValuePair.
     */
    public void reduce(LemoineValuePair lvp) {
        throw new UnsupportedOperationException("reduce() not defined in base class LemoineValuePairVbl; use a subclass");
    }

    /**
     * Reduce the given shared variable into this shared variable. The two
     * variables are combined together using this shared variable's reduction
     * operation, and the result is stored in this shared variable.
     *
     * @param  vbl  Shared variable.
     */
    @Override
    public void reduce(Vbl vbl) {
        reduce(((LemoineValuePairVbl) vbl).getItem());
    }

    /**
     * String representation of the stored LemoineValuePair Object.
     * @return string representation
     */
    public String toString() {
        return this.getItem().toString();
    }

    // Exported classes
    
    /**
     * Class LemoineValuePair.Min provides a reduction variable for a pair of
     * LemoineValuePair objects, where the reduction operation is to keep the 
     * set with the smallest value. The objects are stored in a local 
     * LemoineValuePair object.
     */
    public static class Min
            extends LemoineValuePairVbl {

        /**
	 * Construct a new LemoineValuePair reduction variable with a 
         * null object
	 */
        public Min() {
            super();
        }

        /**
	 * Construct a new LemoineValuePair reduction variable 
         * with a given, initial LemoineValuePair object.
	 */
        public Min(LemoineValuePair lvp) {
            super(lvp);
        }

        /**
	 * Reduce the given LemoineValuePair into this shared variable. 
         * This shared variable's lvp field and the <TT>lvp</TT>
	 * argument are compared together using this shared variable's 
         * reduction, the minimum of the two is found using the compare
	 * operation, and the result is stored in the LemoineValuePair
	 * field.
	 *
	 * @param lvp LemoineValuePair.
	 */
        public void reduce(LemoineValuePair lvp) {
            this.setItem(LemoineSmp.compareLemoine(this.getItem(), lvp));
        }
        
    }

}
