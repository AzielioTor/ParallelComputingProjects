// Imports
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;
import static edu.rit.pj2.Task.terminate;

/**
 * Calculates the lemonine conjecture with the lowest p 
 * through a range of numbers in parallel. Returns the
 * n of all numbers with the highest p.
 * @author Aziel Shaw
 * @version 18-Sept-2018
 */
public class LemoineSmp extends Task {
    
    // Set up global tuple
    private LemoineValuePairVbl.Min lvpVbl;
    
    /**
     * Launches the Parallel Lemoine Conjecture Program
     * @param args The <lower> and <upper> bounds at which to iterate over
     * @throws Exception Throws an Exception if an error pops up
     */
    @Override
    public void main(String[] args) throws Exception {
        if(args.length < 2) {
            System.out.println("LemoineSmp: Must have 2 arguments passed in, <lb>, <ub>");
            usage();
        }
        // Cast commandline arguments to ints
        int lb = parseInput(args[0]);
        int ub = parseInput(args[1]);
        // Check commandline arguments worked correctly
        if(lb == -1 || ub == -1) {
            usage();
        }
        // Check that <ub> is larger than <lb>
        else if(lb > ub) {
            System.err.println("LemoineSmp: <ub> must be >= <lb>");
            usage();
        }
        // Check that <lb> is greater than or equal to 5
        else if(lb <= 5) {
            System.err.println("LemoineSmp: <lb> must be > 5");
            usage();
        }
        // Check that <lb> is an odd number
        else if(lb % 2 == 0) {
            System.err.println("LemoineSmp: <lb> must be odd");
            usage();
        }
        // Check that <ub> is an odd number
        else if(ub % 2 == 0) {
            System.err.println("LemoineSmp: <ub> must be odd");
            usage();
        }
        // Create Reduction object
        lvpVbl = new LemoineValuePairVbl.Min(null);
        // Iterate through bounds
        parallelFor(lb, ub)
                .schedule(dynamic)
                .exec(new Loop() {
            // Create threadLocal variable
            LemoineValuePairVbl thrlvpVbl;
            public void start() throws Exception {
                thrlvpVbl = threadLocal(lvpVbl);
            }
            // Run method
            @Override
            public void run(int i) throws Exception {
                if(i % 2 == 1) {
                    thrlvpVbl.reduce(lemoine(i));
                }
            }
        });
        // Print result
        System.out.println(lvpVbl);
    }
    
    /**
     * Perform the Lemoine Conjecture on a number
     * @param num Number to check for p + 2*q
     * @return n, p, and q that satisfies n = p + 2*q. 
     *         Where p is the smallest value to satisfy.
     */
    private static LemoineValuePair lemoine(int num) {
        Prime.Iterator primeIterator = new Prime.Iterator();
        int p, q;
        if(num > 5) {
            while(true) {
                // Iterate until a value is found to satisfy Lemoines
                p = primeIterator.next();
                q = (num - p) / 2;
                // If both numbers are prime, return
                if(Prime.isPrime(q) && Prime.isPrime(p)) {
                    return new LemoineValuePair(num, p, q);
                }
            }
        }
        return  null;
    }
    
    /**
     * Takes input and returns the longs if proper.
     * If not proper it catches the thrown exceptions and exits.
     * @param arg The string argument to convert into an int
     * @return The parsed integer
     */
    private static int parseInput(String arg) {
        int result = -1;
        try {
            result = Integer.parseInt(arg);
        } catch(NumberFormatException nfe) {
            System.err.println("Arguments passed in must be integers");
        } catch(Exception e) {
            System.err.println("Arguments passed in must be integers");
        } finally {
            return result;
        }
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("Usage: java pj2 cores=<K> "
                + "jar=PCP1.jar lemoine.LemoineSmp <lb> <ub>");
        terminate(1);
    }
    
    /**
     * Compares to Lemoine objects
     * @param p1 Object 1 to compare to Object 2
     * @param p2 The object to be compared to Object 1
     * @return The object with the lower p value
     */
    public static LemoineValuePair compareLemoine(LemoineValuePair p1, LemoineValuePair p2) {
        // Null checks for p1 and p2.
        if(p1 == null) {
            if(p2 == null) {
                return null;
            }
            return p2;
        } else if(p2 == null) {
            return p1;
        }
        // Perform comparision
        if(p1.compareTo(p2) == 1) {
            return p1;
        } else if(p1.compareTo(p2) == -1) {
            return p2;
        } else {
            return p2;
        }
    }
    
}