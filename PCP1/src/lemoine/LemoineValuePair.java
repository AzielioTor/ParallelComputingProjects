/**
 * A custom tuple object used to store the information regarding the 
 * Lemoine values returned.
 * @author Aziel Shaw
 * @version 18-Sept-2018
 */
public class LemoineValuePair implements Comparable<LemoineValuePair> {
    // Local variables which store the Lemoine Conjecture information
    // n = p + 2*q
    private int n;
    private int p;
    private int q;

    /**
     * The constructor for this object. Must take in n, p, & q.
     * @param n The n lemoine value.
     * @param p The p lemoine value.
     * @param q The q lemoine value.
     */
    public LemoineValuePair(int n, int p, int q) {
        this.n = n;
        this.p = p;
        this.q = q;
    }
    
    /**
     * Returns the n value associated with this object.
     * @return The n value.
     */
    public int getN() {
        return this.n;
    }
    
    /**
     * Returns the p value associated with this object.
     * @return The p value.
     */
    public int getP() {
        return this.p;
    }
    
    /**
     * Returns the q value associated with this object.
     * @return The q value.
     */
    public int getQ() {
        return this.q;
    }

    /**
     * Overrides the compareTo method from the Comparable interface.
     * @param o The object to compare to.
     * @return An in representing the result of the comparison.
     */
    @Override
    public int compareTo(LemoineValuePair o) {
        if(this.p < o.getP()) {
            return -1;
        } else if(this.p > o.getP()) {
            return 1;
        } else {
            if(this.getN() < o.getN()) {
                return -1;
            } else if(this.getN() > o.getN()) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
    /**
     * Returns a string representation of this tuple object.
     * @return The string representation of this tuple object.
     */
    @Override
    public String toString() {
        return this.getN() + " = " + this.getP() + " + 2*" + this.getQ();
    }
    
}
