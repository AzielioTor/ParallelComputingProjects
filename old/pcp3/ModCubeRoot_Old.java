import edu.rit.gpu.Gpu;
import edu.rit.gpu.Kernel;
import edu.rit.pj2.Task;

/**
 * DONT EDIT THIS!!!!!!
 * @author azieliotor
 */
public class ModCubeRoot_Old extends Task {

    /**
     * Kernel function interface.
     */
    private static interface ModCubeRootKernel extends Kernel {

        public void computeModCubeRoot(int c, int n);
    }

    /**
     * Task Main program
     *
     * @param strings Commandline arguments
     * @throws Exception Throws Exception
     */
    @Override
    public void main(String[] args) throws Exception {
        // Validate command line arguments.
        if (args.length != 2) {
            System.out.println("Two arguments required.");
            usage();
        }
        // Validate command line arguments.
        int c;
        int n;
        try {
            c = Integer.parseInt(args[0]);
            n = Integer.parseInt(args[1]);
        } catch (NumberFormatException nfe) {
            System.out.println("Arguments passed in must be of type INT");
            usage();
        }
        // Initialize GPU.
        Gpu gpu = Gpu.gpu();
        gpu.ensureComputeCapability(2, 0);
    }

    /**
     * Print a usage message and exit.
     */
    private static void usage() {
        System.err.println("Usage: java pj2 ModCubeRoot <c> <n>");
        System.err.println("<c> = is the number whose modular cube root(s) are to be found;\n"
                + "      it must be a decimal integer (type int) in the range 0 ≤ c ≤ n−1.");
        System.err.println("<n> = is the modulus; it must be a decimal integer (type int) ≥ 2.");
        terminate(1);
    }

    /**
     * Specify that this task requires one core.
     */
    protected static int coresRequired() {
        return 1;
    }

    /**
     * Specify that this task requires one GPU accelerator.
     */
    protected static int gpusRequired() {
        return 1;
    }

}
