import edu.rit.gpu.Gpu;
import edu.rit.gpu.Module;
import edu.rit.pj2.Task;
import static edu.rit.pj2.Task.terminate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author azieliotor
 */
public class MCR extends Task {
    
    private final int E = 3;
    
    @Override
    public void main(String[] args) throws Exception {
        // Validate command line arguments.
        if (args.length != 2) {
            System.out.println("Two arguments required.");
            usage();
        }
        // Validate command line arguments.
        int c = 0;
        int n = 0;
        try {
            c = Integer.parseInt(args[0]);
            n = Integer.parseInt(args[1]);
        } catch (NumberFormatException nfe) {
            System.out.println("Arguments passed in must be of type INT");
            usage();
        }
        // Start mod cube root.
        ArrayList<MCR_Tuple> arraylist = computeMCR(new ArrayList<MCR_Tuple>(), c, n);
        if(arraylist.isEmpty()) {
            System.out.println("No cube roots of " + c + " (mod " + n + ")");
        } else {
            for(int i = 0; i < arraylist.size(); i++) {
                int newM = arraylist.get(i).m;
                int newC = arraylist.get(i).c;
                int newP = arraylist.get(i).p;
                System.out.println(newM + "^3 = " + newC + " (mod " + newP + ")");
            }
        }
    }
    
    private ArrayList<MCR_Tuple> computeMCR(ArrayList<MCR_Tuple> arraylist, int c, int n) {
        int result = 0;
        for(int m = 0; m < n; m++) {
            result = multiply(m, n);
            if(result == c)
                arraylist.add(new MCR_Tuple(m, c, n));
        }
        return arraylist;
    }
    
    private int squareAndMultiply(int x, int e, int m) {
        // Convert e to binary
        String binary_e = Integer.toBinaryString(e);
        // Start Square and Multiply
        int result = 1;
        int elength = binary_e.length();
        for(int i = 0; i < elength; i++) {
            result = (int) (Math.pow(x, 2)) % m;
            if(binary_e.charAt(i) == '1')
                result = (result * x) % m;
        }
        // Return result
        return result;
    }
    
    /**
     * Takes in 2 numbers and returns x^3 mod m
     * @param x The base to 
     * @param m
     * @return 
     */
    private int multiply(int x, int m) {
        // Convert e to binary
        int result = x;
        result = (result * x) % m;
        result = (result * x) % m;
        return result;
    }
    
    /**
     * Print a usage message and exit.
     */
    private static void usage() {
        System.err.println("Usage: java pj2 ModCubeRoot <c> <n>");
        System.err.println("<c> = is the number whose modular cube root(s) are to be found;\n"
                + "      it must be a decimal integer (type int) in the range 0 ≤ c ≤ n−1.");
        System.err.println("<n> = is the modulus; it must be a decimal integer (type int) ≥ 2.");
        terminate(1);
    }

    /**
     * Specify that this task requires one core.
     */
    protected static int coresRequired() {
        return 1;
    }

    /**
     * Specify that this task requires one GPU accelerator.
     */
    protected static int gpusRequired() {
        return 1;
    }
}
