/**
 * This class is the gpu program for ModCubeRoot. It takes in 2 arguments.
 * These two arguments are the target c to fine, and the modulus m.
 * 
 * @author Aziel Shaw - 809000893
 * @version November 8th, 2018
 */


// Number of threads per block.
#define NT 1024

// Global variables
__device__ unsigned long long int devCounter; // Current index to put into global long array
__device__ unsigned long long int devArray [3]; // Global long array to store results

// Method headers and logic
/**
 * Takes in two numbers, cubes it, and returns the mod value
 * @param x The number to cube
 * @param m The number to mod
 * @return The value x^3 mod m
 */
__device__ unsigned long long int multiply(unsigned long long int x, unsigned long long int m) {
    return ((((x * x) % m) * x) % m);
}

/**
 * Main method for 
 * @param c The result to find
 * @param m The number to mod
 */
extern "C" __global__ void computeModCubeRoot
   (unsigned long long int c,
    unsigned long long int n) {
    int thr, size, thrRank;

    // Determine number of threads and this thread's rank.
    thr = threadIdx.x;
    size = gridDim.x*NT;
    thrRank = blockIdx.x*NT + thr;
    
    // Create Variable to store result
    unsigned long long int modCubeRootResult = 0;
    // Iterate between thread bounds to find result
    for(unsigned long long int m = thrRank; m < n; m = m + size) {
        // Store result
        modCubeRootResult = multiply(m, n);
        // If result = the value we're trying to find
        if (modCubeRootResult == c){
            // Increment counter and store old result
            unsigned long long int old_value = atomicAdd(&devCounter, 1);
            // Use old result to store into global array
            devArray[old_value] = m;
        }
    }
}
