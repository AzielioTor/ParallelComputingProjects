// Imports
import edu.rit.gpu.Gpu;
import edu.rit.gpu.GpuIntVbl;
import edu.rit.gpu.GpuLongArray;
import edu.rit.gpu.Kernel;
import edu.rit.gpu.Module;
import edu.rit.pj2.Task;
import java.util.Arrays;

/**
 * This class ModCubeRoot handles the calculation of modulus cube roots based 
 * on a given C value (the number we're trying to find), and N value (the mod)
 * This class prints out the 1-3 results based on given numbers. If no result
 * is found, that is also printed.
 * @author Aziel Shaw
 * @version November 8th, 2018
 */
public class ModCubeRoot extends Task {
        
    /**
     * Kernel function interface.
     */
    private static interface ModCubeRootKernel extends Kernel {
        public void computeModCubeRoot(int c, int n);
    }

    /**
     * Task Main program
     *
     * @param args Commandline arguments
     * @throws Exception Throws Exception
     */
    @Override
    public void main(String[] args) throws Exception {
        // Validate command line arguments. Should be 2
        if (args.length != 2) {
            System.out.println("Two integer arguments required.");
            usage();
        }
        
        // Validate command line arguments. Should be Integers
        int c = 0;
        int n = 0;
        try {
            c = Integer.parseInt(args[0]);
            n = Integer.parseInt(args[1]);
        } catch (NumberFormatException nfe) {
            System.out.println("Arguments passed in must be of type INT");
            usage();
        }
        // Validate command line arguments. C should be less than N
        if(c > n) {
            System.out.println("C cannot be greater than N");
            usage();
        }
        // Validate command line arguments. N must be greater than 1
        if(n < 2) {
            System.out.println("N must be >= 2");
            usage();
        }
        // Validate command line arguments. C must be greater than 1
        if(0 > c || c > n - 1) {
            System.out.println("C must be 0 <= c <= n−1");
            usage();
        }
        // Initialize GPU.
        Gpu gpu = Gpu.gpu();
        gpu.ensureComputeCapability(2, 0);
        // Set up GPU counter variable.
        Module module = gpu.getModule ("ModCubeRoot.ptx");
        // Set up global counter and long array
        GpuLongArray devArray = module.getLongArray("devArray", 3);
        GpuIntVbl devCounter = module.getIntVbl("devCounter");
        // Set counter to 0, and send to global GPU memory
        devCounter.item = 0;
        devCounter.hostToDev();
        devArray.hostToDev();
        // Setup Kernal
        ModCubeRootKernel kernel = module.getKernel (ModCubeRootKernel.class);
        kernel.setBlockDim (1024); // Set block dim
        kernel.setGridDim (gpu.getMultiprocessorCount()); // Set Grid Dim
        kernel.computeModCubeRoot(c, n); // Start GPU program
        
        // When GPU program is finished running, retrieve results
        devArray.devToHost();
        // Store result into long[]
        long[] solutions = devArray.item;
        Arrays.sort(solutions); // Sort solutions
        boolean flag = false;
        for(int i = 0; i < solutions.length; i++) {
            // If there is a solution which is 0, means it isn't correct, don't print
            if(solutions[i] !=  0) {
                System.out.println(solutions[i] + "^3 = " + c + " (mod " + n + ")");
                flag = true;
            }
        }
        // If no results are found, print message
        if(!flag)
            System.out.println("No cube roots of " + c + " (mod " + n + ")");
    }

    /**
     * Print a usage message and exit.
     */
    private static void usage() {
        System.err.println("Usage: java pj2 ModCubeRoot <c> <n>");
        System.err.println("<c> = is the number whose modular cube root(s) are to be found;\n"
                + "      it must be a decimal integer (type int) in the range 0 ≤ c ≤ n−1.");
        System.err.println("<n> = is the modulus; it must be a decimal integer (type int) ≥ 2.");
        terminate(1);
    }

    /**
     * Specify that this task requires one core.
     */
    protected static int coresRequired() {
        return 1;
    }

    /**
     * Specify that this task requires one GPU accelerator.
     */
    protected static int gpusRequired() {
        return 1;
    }

}
