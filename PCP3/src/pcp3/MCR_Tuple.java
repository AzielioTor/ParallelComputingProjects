/**
 *
 * @author azieliotor
 */
public class MCR_Tuple {
    public int m;
    public int c;
    public int p;
    
    public MCR_Tuple() {
        
    }
    
    public MCR_Tuple(int m, int c, int p) {
        this.m = m;
        this.c = c;
        this.p = p;
    }
}
