// Imports
import edu.rit.pj2.Job;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;
import edu.rit.util.Instance;

/**
 * Calculates the largest Triangle with the largest area or lowest indexes
 * if the areas are the same through a range of numbers on a cluster. Returns 
 * the "Largest" Triangle.
 * @author Aziel Shaw - 809000893 - ats9095
 * @version October 14th, 2018
 */
public class LargestTriangleClu extends Job {
    
    @Override
    public void main(String[] args) throws Exception {
        // Check commandline arguments
        if(!(args[0] instanceof String)) {
            System.out.println("Argument passed in must be string");
            usage();
        }
        if(args.length < 1) {
            System.out.println("Requires 1 string argument");
            usage();
        }
        // Get the commandline arguments
        String psobj = args[0];
        PointSpec pso = null;
        try {
            pso = (PointSpec) Instance.newInstance(psobj);
        } catch(Exception e) {
            System.out.println("Object must extend PointSpec object");
            usage();
        }
        // WORKER FOR
        // Set up a task group of K worker tasks.
        masterSchedule(proportional);
        masterChunk(10);
        masterFor(0, pso.size() - 2, LargestTriangleWorkerTask.class).schedule(proportional).args(psobj);
        
        // Set up reduction task.
        rule().atFinish().task(TriangleReduceTask.class).args().runInJobProcess();
        
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("java pj2 jar=<jarfile> workers=<K> "
                + "LargestTriangleClu \"<pointspec>\"");
        terminate(1);
    }
    
    // Task Subclass
    
    /**
    * Class LargestTriangleClu.LargestTriangleWorkerTask provides the Task 
    * that computes Largest Triangle given a bunch of points.
    *
    * @author  Aziel Shaw
    * @version October 9th, 2018
    */
    private static class LargestTriangleWorkerTask extends Task {
        // Local TriangleVbl object
        private TriangleVbl trivbl;
        private Point[] pointarray;
        
        /**
         * Main method for each worker
         * @param args Arguments for worker class
         * @throws Exception Throws exception if something goes wrong
         */
        @Override
        public void main(String[] args) throws Exception {
            // Construct the TriangleVbl variable
            trivbl = new TriangleVbl.Max();
            String psobj = args[0];
            // If point spec was successfully created in Parent Main then this is safe to run.
            PointSpec pso = (PointSpec) Instance.newInstance(psobj);
            pointarray = new Point[pso.size()];
            int counter = 0;
            while(pso.hasNext()) {
                Point p = pso.next();
                Point newp = new Point();
                newp.x = p.x;
                newp.y = p.y;
                pointarray[counter++] = newp;
            }
            // Start the WorkerFor loop
            workerFor().exec (new Loop() {
                TriangleVbl thrtrivbl;
                
                /**
                 * Initialized the workerforloop objects.
                 * @throws ClassNotFoundException If class not found, throws exception
                 */
                @Override
                public void start() throws ClassNotFoundException {
                    thrtrivbl = threadLocal(trivbl);
                }
                
                /**
                 * Main loop of the worker. Performs area calculation and checks
                 * @param i number to perform operations. Index of array to check.
                 * @throws Exception Throws exception if something goes wrong.
                 */
                @Override
                public void run(int i) throws Exception {
                    int pa_len = pointarray.length;
                    for(int j = i+1; j < pa_len - 1; ++j)
                        for(int k = j+1; k < pa_len; ++k) {
                            Point a = pointarray[i];
                            Point b = pointarray[j];
                            Point c = pointarray[k];
                            double ab = Triangle.euclidianDistance(a, b);
                            double bc = Triangle.euclidianDistance(b, c);
                            double ac = Triangle.euclidianDistance(a, c);
                            double newArea = Triangle.triangleArea(((ab + bc + ac) /2), ab, bc, ac);
                            if(newArea >= thrtrivbl.getItem().getArea())
                                thrtrivbl.reduce(Triangle.createTriangle(pointarray[i], i, pointarray[j], j, pointarray[k], k));
                        }
                }
            }); // End workerFor loop
            // Put the resultant TriangleVbl into TupleSpace
            putTuple(trivbl);
        }
        
    }

    // Reduction class
    
    /**
    * Class LargestTriangleClu.TriangleReduceTask combines the worker 
    * tasks' results and prints the overall result for 
    * the LargestTriangleClu program.
    *
    * @author Aziel Shaw
    * @version October 9th, 2018
    */
    private static class TriangleReduceTask extends Task {

        /**
         * Reduce task main program.
         */
        public void main(String[] args) throws Exception {
            // Create objects to help reduction.
            TriangleVbl tri = new TriangleVbl.Max();
            TriangleVbl template = new TriangleVbl();
            TriangleVbl taskCount;
            while ((taskCount = tryToTakeTuple(template)) != null) {
                tri.reduce(taskCount);
            }
            Triangle result = tri.getItem();
            // Print Required Result
            int index = result.getAIndex();
            double x = result.getA().x;
            double y = result.getA().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            index = result.getBIndex();
            x = result.getB().x;
            y = result.getB().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            index = result.getCIndex();
            x = result.getC().x;
            y = result.getC().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            double area = result.getArea();
            System.out.printf ("%.5g%n", area);
        }
    }

}
