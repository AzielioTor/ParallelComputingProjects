// Imports
import edu.rit.io.InStream;
import edu.rit.io.OutStream;
import edu.rit.pj2.Tuple;
import edu.rit.pj2.Vbl;
import java.io.IOException;

/**
 * Provides a global reduction variable for a Triangles stored
 * for the purpose of helping find the largest triangle based on points
 * @author Aziel Shaw - 809000893 - ats9095
 * @version October 14th, 2018
 */
public class TriangleVbl extends Tuple implements Vbl {

    // Kludge to avert false sharing in multithreaded programs.
    // Padding fields.
    volatile long p0 = 1000L;
    volatile long p1 = 1001L;
    volatile long p2 = 1002L;
    volatile long p3 = 1003L;
    volatile long p4 = 1004L;
    volatile long p5 = 1005L;
    volatile long p6 = 1006L;
    volatile long p7 = 1007L;
    volatile long p8 = 1008L;
    volatile long p9 = 1009L;
    volatile long pa = 1010L;
    volatile long pb = 1011L;
    volatile long pc = 1012L;
    volatile long pd = 1013L;
    volatile long pe = 1014L;
    volatile long pf = 1015L;

    // Method to prevent the JDK from optimizing away the padding fields.
    long preventOptimization() {
        return p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7
                + p8 + p9 + pa + pb + pc + pd + pe + pf;
    }
    
    /**
     * Protected local Triangle Object
     */
    protected Triangle tri;
    
    /**
     * Instantiates the class with a "null" Triangle
     */
    public TriangleVbl() {
        this(new Triangle());
    }
    
    /**
     * Instantiates the class with a given Triangle
     * @param tri The triangle to set
     */
    public TriangleVbl(Triangle tri) {
        this.tri = tri;
    }
    
    /**
     * Returns the local Triangle object
     * @return The local Triangle object
     */
    public Triangle getItem() {
        return this.tri;
    }
    
    /**
     * Sets the local Triangle object to a given Triangle
     * @param tri The Triangle to set
     */
    public void setItem(Triangle tri) {
        this.tri = tri;
    }
    
    /**
     * Write this Triangle reduction variable to the given out stream.
     * @param out Out Stream
     * @throws IOException Thrown if an I/O error occurred.
     */
    @Override
    public void writeOut(OutStream out) throws IOException {
        out.writeObject(this.getItem());
    }
    
    /**
     * Read this Triangle reduction variable from the given in stream.
     * @param in In stream.
     * @throws IOException Thrown if an I/O error occurred.
     */
    @Override
    public void readIn(InStream in) throws IOException {
        this.setItem((Triangle) in.readObject());
    }

    /**
     * Set this shared variable to the given shared variable.
     * @param vbl Shared variable.
     */
    @Override
    public void set(Vbl vbl) {
        this.setItem(((TriangleVbl) vbl).getItem());
    }

    /**
     * Reduce the given shared variable into this shared variable. The two
     * variables are combined together using this shared variable's reduction
     * operation, and the result is stored in this shared variable.
     *
     * @param  vbl  Shared variable.
     */
    @Override
    public void reduce(Vbl vbl) {
        reduce(((TriangleVbl) vbl).getItem());
    }
    
    /**
     * Reduce the given Triangle Object into this shared variable. 
     * This shared variable's Triangle field and the 
     * <TT>tri</TT> argument are combined together using this 
     * shared variable's reduction operation, and
     * the result is stored in the local tri field.
     * <P>
     * The base class <TT>reduce()</TT> method throws an exception. The
     * reduction operation must be defined in a subclass's <TT>reduce()</TT>
     * method.
     *
     * @param  tri  LemoineValuePair.
     */
    public void reduce(Triangle tri) {
        throw new UnsupportedOperationException("reduce() not defined in base class TriangleVbl; use a subclass");
    }
    
    /**
     * String representation of the stored Triangle Object.
     * @return string representation
     */
    @Override
    public String toString() {
        return this.getItem().toString();
    }
    
    // Exported Classes
    
    /**
     * Class Triangle.Max provides a reduction variable for a pair of
     * Triangle objects, where the reduction operation is to keep the 
     * Triangle with the Largest value. The objects are stored in a local 
     * Triangle object.
     */
    public static class Max extends TriangleVbl {
        
        /**
	 * Construct a new Triangle reduction variable with a null object
	 */
        public Max() {
            super();
        }
        
        /**
	 * Construct a new Triangle reduction variable 
         * with a given, initial Triangle object.
	 */
        public Max(Triangle tri) {
            super(tri);
        }
        
        /**
	 * Reduce the given Triangle into this shared variable. 
         * This shared variable's tri field and the <TT>tri</TT>
	 * argument are compared together using this shared variable's 
         * reduction, the larger of the two is found using the compare
	 * operation, and the result is stored in the Triangle field.
	 *
	 * @param tri Triangle.
	 */
        public void reduce(Triangle tri) {
            this.setItem(Triangle.triangleMax(this.getItem(), tri));
        }
        
    }
    
}
