// Imports
import edu.rit.io.InStream;
import edu.rit.io.OutStream;
import edu.rit.pj2.Tuple;
import java.io.IOException;

/**
 * A custom Triangle object made to store attributes relating to THIS Triangle
 * object. Such as the 3 points in the 2D plane used to create the Triangle, and
 * the 3 indicies of the points, followed by the total area of the Triangle.
 * 
 * This class also has static methods used for the creation and comparison 
 * of Triangle objects.
 * 
 * @author Aziel Shaw - 809000893 - ats9095
 * @version October 14th, 2018
 */
public class Triangle extends Tuple implements Comparable<Triangle> { //, Streamable {
    
    // Instance Variable Triangle attributes.
    private Point a;
    private Point b;
    private Point c;
    private int a_index;
    private int b_index;
    private int c_index;
    private double area;

    /**
     * Constructor for a "null" Triangle
     */
    public Triangle() {
        this(null, -1, null, -1, null, -1, -1);
    }
    
    /**
     * Constructs a Triangle object based on the points, indicies of the points,
     *  and the area of the Triangle (Which can all be calculated using the 
     * static methods contained within this class)
     * @param a Point 1 of the triangle
     * @param a_index The index of point 1
     * @param b Point 2 of the triangle
     * @param b_index The index of point 2
     * @param c Point 3 of the triangle
     * @param c_index The index of point 3
     * @param area The total area of the triangle
     */
    public Triangle(Point a, int a_index, 
            Point b, int b_index, 
            Point c, int c_index, double area) {
        // Set all variables to values passed in.
        this.a = a;
        this.b = b;
        this.c = c;
        this.a_index = a_index;
        this.b_index = b_index;
        this.c_index = c_index;
        this.area = area;
    }
    
    /**
     * Return point 1
     * @return point 1
     */
    public Point getA() {
        return this.a;
    }
    
    /**
     * Return point 2
     * @return point 2
     */
    public Point getB() {
        return this.b;
    }
    
    /**
     * Return point 3
     * @return point 3
     */
    public Point getC() {
        return this.c;
    }
    
    /**
     * Returns the index of point a/1
     * @return index of point a/1
     */
    public int getAIndex() {
        return this.a_index;
    }
    
    /**
     * Returns the index of point b/2
     * @return index of point b/2
     */
    public int getBIndex() {
        return this.b_index;
    }
    
    /**
     * Returns the index of point c/3
     * @return index of point c/3
     */
    public int getCIndex() {
        return this.c_index;
    }
    
    /**
     * Returns the total area of the Triangle
     * @return The total Area of the Triangle
     */
    public double getArea() {
        return this.area;
    }
    
    /**
     * Takes in another Triangle object and compares it to this one based on 
     * if certain parts of the Triangle are null, then by total Area of each 
     * Triangle, then by the indicies of each point of the Triangle.
     * @param o The other triangle to compare to this object
     * @return 1/0/-1 if the object passed in is greater than/equal/less than
     *  this object respectively.
     */
    @Override
    public int compareTo(Triangle o) {
        /** 
         * Check if either object has null fields, if yes, return values based
         *   on that.
         **/
        if(this.a == null) {
            if(o.a == null) {
                return 0;
            }
            return -1;
        }
        if(o.a == null) {
            return 1;
        }
        
        /**
         * Compares the areas and indicies of the lowest points in the Triangle
         */
        if(this.getArea() < o.getArea()) {
            return -1;
        } else if(this.getArea() > o.getArea()) {
            return 1;
        } else {
            if(this.getAIndex() < o.getAIndex()) {
                return -1;
            } else if(this.getAIndex() > o.getAIndex()) {
                return 1;
            } else {
                if(this.getBIndex() < o.getBIndex()) {
                    return -1;
                } else if(this.getBIndex() > o.getBIndex()) {
                    return 1;
                } else {
                    if(this.getCIndex() < o.getCIndex()) {
                        return -1;
                    } else if(this.getCIndex() > o.getCIndex()) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }
    
    /**
     * Returns a string representation of the Triangle
     * @return A string representation of the Triangle
     */
    @Override
    public String toString() {
        if(this.getA() == null) {
            return "This Triangle is NULL";
        }
        return this.getAIndex() + "  " + this.getA().x + "," + this.getA().y +  " | " 
                + this.getBIndex() + "  " + this.getB().x + "," + this.getB().y +  " | " 
                + this.getCIndex() + "  " + this.getC().x + "," + this.getC().y + 
                "\nAREA=" + this.getArea() + "\n";
    }
    
    /**
     * Local static method used to compare two given triangles and return 
     * the one that is considered "Larger"
     * @param t Triangle 1 to be compared
     * @param r Triangle 2 to be compared
     * @return The "larger" of the two Triangles
     */
    public static Triangle triangleMax(Triangle t, Triangle r) {
        if(t.compareTo(r) == 1) {
            return t;
        } else if(t.compareTo(r) == -1) {
            return r;
        } else {
            return t;
        }
    }
    
    /**
     * Constructs a Triangle and returns it based 
     * @param a Point 1 to construct the Triangle
     * @param a_index The index of point 1
     * @param b Point 2 to construct the Triangle
     * @param b_index The index of point 2
     * @param c Point 3 to construct the Triangle
     * @param c_index The index of point 3
     * @return The constructed Triangle object
     */
    public static Triangle createTriangle(Point a, int a_index, 
            Point b, int b_index, 
            Point c, int c_index) {
        // Get the sides of the triangle
        double ab = euclidianDistance(a, b);
        double bc = euclidianDistance(b, c);
        double ac = euclidianDistance(a, c);
//        double s = triangleSideSize(ab, bc, ac);
        // Return the newly constructed Triangle
        return new Triangle(a, a_index, b, b_index, c, c_index, triangleArea(((ab + bc + ac) /2), ab, bc, ac));
    }
    
    /**
     * Calculates the euclidian distances between two points.
     * @param x Point 1 to calculate euclidian distance
     * @param y Point 2 to calculate euclidian distance
     * @return The distance between the two points
     */
    public static double euclidianDistance(Point x, Point y) {
        double newX = Math.abs(x.x - y.x);
        double newY = Math.abs(x.y - y.y);
        return Math.sqrt((newX * newX) + (newY * newY));
    }
    
    /**
     * Calculates the Triangle Area based on the sides
     * @param s s = (a + b + c)/2
     * @param a Side a
     * @param b Side b
     * @param c Side c
     * @return The total area of the Triangle
     */
    public static double triangleArea(double s, double a, double b, double c) {
        return Math.sqrt(Math.abs(s *((s - a) * (s - b) * (s - c))));
    }
    
    /**
     * Calculates s
     * @param a side a
     * @param b side b
     * @param c side c
     * @return Returns s
     */
    public static double triangleSideSize(double a, double b, double c) {
        return (a + b + c) / 2;
    }

    /**
     * Writes out the object to the given outstream
     * @param out outsteam
     * @throws IOException Thrown if stream invalid 
     */
    @Override
    public void writeOut(OutStream out) throws IOException {
        /// Write out points
          //Point A
        out.writeDouble(getA().x);
        out.writeDouble(getA().y);
          //Point B
        out.writeDouble(getB().x);
        out.writeDouble(getB().y);
          //Point C
        out.writeDouble(getC().x);
        out.writeDouble(getC().y);
        /// Write out Indexes
        out.writeInt(getAIndex());
        out.writeInt(getBIndex());
        out.writeInt(getCIndex());
        /// Write out Area
        out.writeDouble(getArea());
    }

    /**
     * Takes in the object and makes Triangle
     * @param in the instream
     * @throws IOException Thrown if stream invalid
     */
    @Override
    public void readIn(InStream in) throws IOException {
        Point newa = new Point();
        Point newb = new Point();
        Point newc = new Point();
        /// Read in points
          //PointA
        newa.x = in.readDouble();
        newa.y = in.readDouble();
        this.a = newa;
          //PointB
        newb.x = in.readDouble();
        newb.y = in.readDouble();
        this.b = newb;
          //PointC
        newc.x = in.readDouble();
        newc.y = in.readDouble();
        this.c = newc;
        /// Read in Indexes
        this.a_index = in.readInt();
        this.b_index = in.readInt();
        this.c_index = in.readInt();
        /// Read in Area
        this.area = in.readDouble();
    }
    
}
