// Imports
import edu.rit.pj2.Task;
import static edu.rit.pj2.Task.terminate;
import edu.rit.util.Instance;

/**
 * Calculates the largest Triangle with the largest area or lowest indexes
 * if the areas are the same through a range of numbers sequentially. Returns 
 * the "Largest" Triangle
 * @author Aziel Shaw - 809000893 - ats9095
 * @version October 14th, 2018
 */
public class LargestTriangleSeq extends Task {
    
    // global pointspec object
    private PointSpec pso;
    
    /**
     * Launches the Sequential Largest Triangle Program
     * @param args The <PointSpec> subobject used to create all the points
     * @throws Exception Throws an Exception if an error pops up
     */
    @Override
    public void main(String[] args) throws Exception {
        // Check the commandline arguments
        if(!(args[0] instanceof String)) {
            System.out.println("Argument passed in must be string");
            usage();
        }
        if(args.length < 1) {
            System.out.println("Requires 1 string argument");
            usage();
        }
        // Get the commandline arguments and create a Pointspec Object
        String psobj = args[0];
        try {
            pso = (PointSpec) Instance.newInstance(psobj);
        } catch(Exception e) {
            System.out.println("Object must extend PointSpec object");
            usage();
        }
        // Create array of pointers
        Point[] pointarray = new Point[pso.size()];
        // Store all the points in an array
        int counter = 0;
        while(pso.hasNext()) {
            Point p = pso.next();
            Point newp = new Point();
            newp.x = p.x;
            newp.y = p.y;
            pointarray[counter++] = newp;
        }
        
        // Get Length of the pointarray object to calculte the Seq version.
        int pa_len = pointarray.length;
        // Calculate the largest Triangle
        TriangleVbl tri = new TriangleVbl.Max();
        for (int i = 0; i < pa_len - 2; ++i)
            for (int j = i+1; j < pa_len - 1; ++j)
                for (int k = j+1; k < pa_len; ++k) {
                    // Check if current Area is bigger than stored area
                    Point a = pointarray[i];
                    Point b = pointarray[j];
                    Point c = pointarray[k];
                    double ab = Triangle.euclidianDistance(a, b);
                    double bc = Triangle.euclidianDistance(b, c);
                    double ac = Triangle.euclidianDistance(a, c);
                    double newArea = Triangle.triangleArea(((ab + bc + ac) /2), ab, bc, ac);
                    if(newArea >= tri.getItem().getArea())
                        tri.reduce(Triangle.createTriangle(pointarray[i], i, pointarray[j], j, pointarray[k], k));
                }
        // Get the item from the reduction variable
        Triangle result = tri.getItem();
        
        // Print Required Result
        int index = result.getAIndex();
        double x = result.getA().x;
        double y = result.getA().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        index = result.getBIndex();
        x = result.getB().x;
        y = result.getB().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        index = result.getCIndex();
        x = result.getC().x;
        y = result.getC().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        double area = result.getArea();
        System.out.printf ("%.5g%n", area);
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("java pj2 jar=<jarfile> LargestTriangleSeq "
                + "\"<pointspec>\"");
        terminate(1);
    }
    
    /**
     * Specifies that this task requires at least 1 core
     * @return Number of cores required
     */
    protected static int coresRequired() {
        return 1;
    }
    
}
