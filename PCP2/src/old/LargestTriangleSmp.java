/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package old;

import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;
import static edu.rit.pj2.Task.terminate;
import edu.rit.util.Instance;
import pcp2.Point;
import pcp2.PointSpec;
import pcp2.Triangle;
import pcp2.TriangleVbl;

/**
 *
 * @author azieliotor
 */
public class LargestTriangleSmp extends Task {
    
    private PointSpec pso;
    
    /**
     * Launches the Sequential Lemoine Conjecture Program
     * @param args The <lower> and <upper> bounds at which to iterate over
     * @throws Exception Throws an Exception if an error pops up
     */
    @Override
    public void main(String[] args) throws Exception {
        if(!(args[0] instanceof String)) {
            System.out.println("Argument passed in must be string");
            usage();
        }
        if(args.length < 1) {
            System.out.println("Requires 1 string argument");
            usage();
        }
        String psobj = args[0];
        pso = (PointSpec) Instance.newInstance(psobj);
        
        final Point[] pointarray = new Point[pso.size()];
        
        int counter = 0;
        while(pso.hasNext()) {
            Point p = pso.next();
            Point newp = new Point();
            newp.x = p.x;
            newp.y = p.y;
            pointarray[counter++] = newp;
        }
        
        
        final int pa_len = pointarray.length;
//        Triangle result = new Triangle();
//        for (int i = 0; i < pa_len; ++i)
//            for (int j = i+1; j < pa_len; ++j)
//                for (int k = j+1; k < pa_len; ++k) {
//                    Triangle newTri = Triangle.createTriangle(pointarray[i], i, pointarray[j], j, pointarray[k], k);
//                    result = Triangle.triangleMax(result, newTri);
//                }
        
//        TriangleVbl tri = new TriangleVbl.Max();
//        for (int i = 0; i < pa_len; ++i)
//            for (int j = i+1; j < pa_len; ++j)
//                for (int k = j+1; k < pa_len; ++k) {
//                    Triangle newTri = Triangle.createTriangle(pointarray[i], i, pointarray[j], j, pointarray[k], k);
//                    tri.reduce(newTri);
//                }
        final TriangleVbl tri = new TriangleVbl.Max();
        parallelFor(0, pa_len - 1).exec(new Loop() {
            
            TriangleVbl thrtrivbl = new TriangleVbl();
            
            public void start() throws Exception {
                thrtrivbl = threadLocal(tri);
            }
            
            @Override
            public void run(int i) throws Exception {
                 for(int j = i+1; j < pa_len; ++j)
                    for(int k = j+1; k < pa_len; ++k) {
                        Triangle newTri = Triangle.createTriangle(pointarray[i], i, pointarray[j], j, pointarray[k], k);
                        thrtrivbl.reduce(newTri);
                    }
            }
        
        });
        
        
        
        Triangle result = tri.getItem();
        
        // Required Result
        int index = result.getAIndex();
        double x = result.getA().x;
        double y = result.getA().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        index = result.getBIndex();
        x = result.getB().x;
        y = result.getB().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        index = result.getCIndex();
        x = result.getC().x;
        y = result.getC().y;
        System.out.printf ("%d %.5g %.5g%n", index, x, y);
        double area = result.getArea();
        System.out.printf ("%.5g%n", area);
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("java pj2 jar=<jarfile> LargestTriangleSeq "
                + "\"<pointspec>\"");
        terminate(1);
    }
    
    /**
     * Specifies that this task requires at least 1 core
     * @return Number of cores required
     */
    protected static int coresRequired() {
        return 1;
    }
    
}
