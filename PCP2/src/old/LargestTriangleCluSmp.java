/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package old;

import edu.rit.pj2.Job;
import static edu.rit.pj2.Job.terminate;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;
import edu.rit.util.Instance;
import pcp2.Point;
import pcp2.PointSpec;
import pcp2.Triangle;
import pcp2.TriangleVbl;

/**
 *
 * @author azieliotor
 */
public class LargestTriangleCluSmp extends Job {

    private PointSpec pso;
    protected static Point[] pointarray;
    
    @Override
    public void main(String[] args) throws Exception {
        if(!(args[0] instanceof String)) {
            System.out.println("Argument passed in must be string");
            usage();
        }
        if(args.length < 1) {
            System.out.println("Requires 1 string argument");
            usage();
        }
        String psobj = args[0];
        pso = (PointSpec) Instance.newInstance(psobj);
        
        this.pointarray = new Point[pso.size()];
        
        int counter = 0;
        while(pso.hasNext()) {
            Point p = pso.next();
            Point newp = new Point();
            newp.x = p.x;
            newp.y = p.y;
            pointarray[counter++] = newp;
        }
        // WORKER FOR
        // Set up a task group of K worker tasks.
        masterFor(0, this.pointarray.length - 1, LargestTriangleWorkerTask.class).args();
        
        // Set up reduction task.
        rule().atFinish().task(TriangleReduceTask.class).args().runInJobProcess();
        
    }
    
    /**
     * Print a message and exit
     */
    private static void usage() {
        System.err.println ("java pj2 jar=<jarfile> workers=<K> "
                + "LargestTriangleClu \"<pointspec>\"");
        terminate(1);
    }
    
    // Task Subclass
    
    private static class LargestTriangleWorkerTask extends Task {

        private TriangleVbl trivbl;
        
        @Override
        public void main(String[] args) throws Exception {
//            System.out.println(args[0]);
//            System.out.println(args[1]);
            trivbl = new TriangleVbl.Max();
            final int pa_len = pointarray.length;
            
            workerFor().exec (new Loop() {
                @Override
                public void run(int i) throws Exception {
                    final int outerIndex = i;
                    parallelFor(0, pa_len - 1).exec(new Loop() {
                        @Override
                        public void run(int j) throws Exception {
                            for(int k = j+1; k < pa_len; ++k) {
                                Triangle newTri = Triangle.createTriangle(pointarray[outerIndex], outerIndex, pointarray[j], j, pointarray[k], k);
                                trivbl.reduce(newTri);
                            }
                        }

                    }); // End Parallel for
                }
            }); // End Worker for
            putTuple(trivbl);
        }
        
    }

    // Reduction class
    
    private static class TriangleReduceTask extends Task {

        /**
         * Reduce task main program.
         * https://www.cs.rit.edu/~ark/bcbd/java2html.php?file=20
         */
        public void main(String[] args) throws Exception {
            TriangleVbl tri = new TriangleVbl.Max();
            TriangleVbl template = new TriangleVbl();
            TriangleVbl taskCount;
            while ((taskCount = tryToTakeTuple(template)) != null) {
                tri.reduce(taskCount);
            }
            Triangle result = tri.getItem();
            
            // Required Result
            int index = result.getAIndex();
            double x = result.getA().x;
            double y = result.getA().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            index = result.getBIndex();
            x = result.getB().x;
            y = result.getB().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            index = result.getCIndex();
            x = result.getC().x;
            y = result.getC().y;
            System.out.printf ("%d %.5g %.5g%n", index, x, y);
            double area = result.getArea();
            System.out.printf ("%.5g%n", area);
        }
    }

}
